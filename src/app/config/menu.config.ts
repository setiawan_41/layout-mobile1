export const _MENU = [
    {
        name: 'home',
        title: 'Home',
        link: ['/home'],
        img: '../assets/icon/home.svg',
    },
    {
        name: 'profile',
        title: 'Profile',
        link: ['/profile'],
        img: '../assets/icon/person.svg',
    },
    {
        name: 'gallery',
        title: 'Gallery',
        link: ['/gallery'],
        img: '../assets/icon/image.svg',
    },
    {
        name: 'about',
        title: 'About',
        link: ['/about'],
        img: '../assets/icon/info.svg',
    },
]