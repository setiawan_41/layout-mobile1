import { animate, style, transition, trigger } from "@angular/animations";

export const Animations = {
    fadein: trigger('fadein', [
        transition(':enter',[
          style({opacity: 0}),
          animate('1800ms ease-in', style({opacity: 1}))
        ]),
        transition(':leave', [
            animate('1800ms ease-in', style({opacity: 0}))
          ])
      ]),
}