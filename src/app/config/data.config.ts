export const Image = [
    {col:'col-12', img: '../../../assets/image/photo1.jpg'},
    {col:'col-6', img: '../../../assets/image/photo2.jpg'},
    {col:'col-6', img: '../../../assets/image/photo3.jpg'},
    {col:'col-12', img: '../../../assets/image/photo4.jpg'},
    {col:'col-12', img: '../../../assets/image/photo5.jpg'},
    {col:'col-12', img: '../../../assets/image/photo6.jpg'},
    {col:'col-6', img: '../../../assets/image/photo7.jpg'},
    {col:'col-6', img: '../../../assets/image/photo8.jpg'},
    {col:'col-12', img: '../../../assets/image/photo9.jpg'}
]