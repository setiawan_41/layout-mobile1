import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivationStart, Router } from '@angular/router';
import { _MENU } from './config/menu.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public _Menu = _MENU;
  public title:any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { 
    this.router.events.subscribe(event => {
      if (event instanceof ActivationStart) {
         const _ = event.snapshot.url
         this.title = _[0].path;
      }
    })

  }

  ngOnInit(): void {
    this.iniMenu()
  }

  iniMenu() {
    this.title = _MENU[0].title
  }
  onRouter(data:any) {
    this.router.navigate(data.link);
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }
}
