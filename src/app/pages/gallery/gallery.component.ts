import { Component, OnInit } from '@angular/core';
import { Animations } from 'src/app/config/animation';
import { Image } from 'src/app/config/data.config';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  animations: [Animations.fadein]
})
export class GalleryComponent implements OnInit {
  public dataImage:any;
  public isLoding = false;
  constructor() { }

  ngOnInit(): void {
    this.getData()
  }
  
  getData() {
    setTimeout(() => {
      this.isLoding = true;
      this.dataImage = Image
    }, 1500);
  }
}
